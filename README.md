# TheFamily

> 当前为NodeJS完全重写版本

## [We are 伐 木 累 ！](http://family.liuzy88.com/)
- 家族亲人信息管理
- 显示家族树
- 显示生日目录
- 公历农历转换
- 手机浏览一样棒

## 界面预览
- ![生日墙](./preview/wall.png)
- ![家族树](./preview/tree.png)
- ![信息列表](./preview/list.png)
- ![添加界面](./preview/edit.png)

## 运行
```bash
npm i
npm start
open 'http://localhost:1314/'
```

## 开发
```bash
npm i
npm i nodemon -g
vim db/conf.js # useMySQL=false
nodemon
open 'http://127.0.0.1:1314/'
```

## 联系我
* Name：Liuzy
* QQ：416657468
* WeiXin：secondsun
* Email：[liuzy@liuzy88.com](mailto:liuzy@liuzy88.com)
