const path = require('path');
const Sequelize = require('sequelize');

// 使用MySQL还是SQLite
const useMySQL = true;

const sequelize = useMySQL ? new Sequelize('family', 'root', '123456', {
    timezone: '+08:00',
    dialect: 'mysql',
    dialectOptions: {
        charset: "utf8",
        supportBigNumbers: true,
        bigNumberStrings: true
    },
    pool: {
        min: 0,
        max: 5,
        acquire: 30000,
        idle: 10000
    },
}) : new Sequelize('family', '', '', {
    dialect: 'sqlite',
    storage: path.join(__dirname, '../family.db'),
});

module.exports.useMySQL = useMySQL;
module.exports.sequelize = sequelize;

module.exports.define = function (name, attributes) {
    const attrs = {};
    for (let key in attributes) {
        let value = attributes[key];
        if (typeof value === 'object' && value['type']) {
            value.allowNull = value.allowNull || false;
            attrs[key] = value;
        } else {
            attrs[key] = {type: value};
        }
    }
    return sequelize.define(name, attrs, {
        tableName: name,
        timestamps: false,
        charset: 'utf8',
    });
};
