const DT = require('./dt');

const EJS = {};

EJS.sex = function(v) {
    return (v === 'M' || v === '男') ? '男' : '女';
};

EJS.person = function(person) {
    person.sex = EJS.sex(person.sex);
    person.gongStr = `${DT.getGongStr(person.gong)}「${DT.getXinZuo(person.gong)}」`;
    person.nongStr = `${DT.getNongStr(person.nong, person.nian)} 属${DT.getShuXian(person.nian)}`;
    person.age = DT.getAge(person);
    const arrN = person.nong.split('-');
    person.nongY = DT.getNian(arrN[0]);
    person.nongM = DT.getYue(arrN[1]);
    person.nongD = DT.getRi(arrN[2]);
    const arrG = person.gong.split('-');
    person.gongY = arrG[0];
    person.gongM = arrG[1];
    person.gongD = arrG[2];
    person.gongW = DT.getWeek(person.gong);
    person.f = {};
    person.m = {};
    person.x = {};
    return person;
};

EJS.info = function(person) {
    if (!person.id) return '';
    const p = EJS.person(person);
    return `${p.name} ${p.sex} ${p.age}岁 ${p.nongStr}`;
};

EJS.calendar = function(calendar) {
    calendar.gongStr = `${DT.getGongStr(calendar.gong)}`;
    calendar.nongStr = `${DT.getNongStr(calendar.nong, calendar.nian)} ${DT.getShuXian(calendar.nian)}年`;
    return calendar;
};

module.exports = EJS;