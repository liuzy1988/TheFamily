const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const co = require('co');

const DB = require('../db');
const DT = require('../lib/dt');
const EJS = require('../lib/ejs');

// 查日历
router.get('/', function (req, res, next) {
    co(function* () {
        const nong = req.query.nong; // 农历
        if (nong) {
            const calendar = yield DB.Calendar.findOne({
                raw: true,
                where: {
                    nong: nong,
                },
            });
            res.json(EJS.calendar(calendar));
        }
        res.json({});
    }).catch((err) => {
        next(err)
    })
});

// 根据农历年和月查天数
router.get('/nong/day', function (req, res, next) {
    co(function* () {
        const nong = req.query.nong; // 2019-9-
        if (nong) {
            const count = yield DB.Calendar.count({
                raw: true,
                where: {
                    nong: {
                        [Op.startsWith]: nong,
                    }
                },
            });
            res.end(count.toString());
        } else {
            res.end('0');
        }
    }).catch((err) => {
        next(err)
    })
});

module.exports = router;
